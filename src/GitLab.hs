{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds, TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications #-}

module GitLab where

import qualified Data.Set           as S
import           Network.HTTP.Client.TLS as TLS
import           Network.HTTP.Types.Status ( statusCode )
import           Data.Text          ( Text )
import           Data.String        ( IsString )
import           Data.Default       ( def )
import           Data.Proxy
import           Data.Aeson
import           Servant.API
import           Servant.Client     as Serv
import           Control.Monad.IO.Class

import           Git                ( Date(..) )

--
--  GitLab
--

----------------------------------------------------------------------
-- Common
----------------------------------------------------------------------

type GitLabRoot = Header "Private-Token" AccessToken

newtype IssueIid = IssueIid { unIssueIid :: Int }
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype UserId = UserId { getUserId :: Int }
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype Weight = Weight Int
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype AccessToken = AccessToken Text
    deriving (Eq, Ord, Show, ToHttpApiData, IsString)

newtype MilestoneId = MilestoneId Int
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype IssueLinkId = IssueLinkId Int
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype ProjectId = ProjectId Int
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype NoteId = NoteId Int
    deriving (Eq, Ord, Show, ToJSON, FromJSON, ToHttpApiData)

newtype Labels = Labels (S.Set Text)
    deriving (Semigroup, Monoid, Show)

----------------------------------------------------------------------
-- Get Project
----------------------------------------------------------------------

type GetProjectAPI =
    GitLabRoot :> "projects"
    :> Capture "id" ProjectId
    :> Get '[JSON] ProjectResp

data ProjectResp = ProjectResp
    { prjRepoHttpUrl    :: Text
    , prjWebUrl         :: Text
    }

instance FromJSON ProjectResp where
    parseJSON = withObject "project" $ \o ->
        ProjectResp
            <$> o .: "http_url_to_repo"
            <*> o .: "web_url"

getProject :: AccessToken -> ProjectId -> ClientM ProjectResp
getProject tok prj =
    liftIO (putStrLn $ "getProject: " ++ show prj)
    >> client (Proxy :: Proxy GetProjectAPI) (Just tok) prj

----------------------------------------------------------------------
-- Get Issue Notes
----------------------------------------------------------------------

type GetIssueNotesAPI =
    GitLabRoot :> "projects"
    :> Capture "id" ProjectId :> "issues"
    :> Capture "iid" IssueIid :> "notes"
    :> Get '[JSON] IssueNotesResp

type IssueNotesResp = [Note]

data Note = Note
    { noteId :: NoteId
    , noteBody   :: Text
    }
    deriving (Show)

instance FromJSON Note where
    parseJSON = withObject "note" $ \o ->
        Note
            <$> o .: "id"
            <*> o .: "body"

getIssueNotes :: AccessToken -> ProjectId -> IssueIid -> ClientM IssueNotesResp
getIssueNotes tok prj iid =
    liftIO (putStrLn $ "getIssueNotes: " ++ show iid)
    >> client (Proxy :: Proxy GetIssueNotesAPI) (Just tok) prj iid

----------------------------------------------------------------------
-- Create Issue Notes
----------------------------------------------------------------------

type CreateNotesAPI =
    GitLabRoot :> "projects"
    :> Capture "id" ProjectId :> "issues"
    :> Capture "iid" IssueIid :> "notes"
    :> ReqBody '[JSON] CreateNoteReq
    :> Post '[JSON] CreateNoteResp

data CreateNoteReq
    = CreateNoteReq
        Text    -- ^ Body of the note
        Date    -- ^ Author date equal of the referenced commit.
    deriving (Show)

data CreateNoteResp = CreateNoteResp

instance FromJSON CreateNoteResp where
    parseJSON = withObject "create note resp" $ \ _ -> return CreateNoteResp

instance ToJSON CreateNoteReq where
    toJSON (CreateNoteReq body (Date date)) = object
        [ "body"        .= body
        , "created_at"  .= date
        ]

createIssueNote :: AccessToken -> ProjectId -> IssueIid -> Text -> Date -> ClientM CreateNoteResp
createIssueNote tok prj iid body date =
    liftIO (putStrLn $ "createIssueNote:" ++ show iid)
    >> client (Proxy :: Proxy CreateNotesAPI) (Just tok) prj iid (CreateNoteReq body date)

----------------------------------------------------------------------
-- Delete Issue Notes
----------------------------------------------------------------------

type DeleteNotesAPI =
    GitLabRoot :> "projects"
    :> Capture "id" ProjectId :> "issues"
    :> Capture "iid" IssueIid :> "notes"
    :> Capture "note_id" NoteId
    :> Delete '[JSON] NoContent

-- data DeleteNoteResp = DeleteNoteResp

-- instance FromJSON DeleteNoteResp where
--     parseJSON = withObject "create note resp" $ \ _ -> return DeleteNoteResp

deleteIssueNote :: AccessToken -> ProjectId -> IssueIid -> NoteId -> ClientM ()
deleteIssueNote tok prj iid nid = do
    liftIO (putStrLn $ "deleteIssueNote:" ++ show iid)
    NoContent <- client (Proxy :: Proxy DeleteNotesAPI) (Just tok) prj iid nid
    return ()

----------------------------------------------------------------------
-- GitLab Environment
----------------------------------------------------------------------

makeGitLabEnv :: BaseUrl -> IO ClientEnv
makeGitLabEnv gitlabApiBaseUrl = do
    mgr <- TLS.newTlsManagerWith $ TLS.mkManagerSettings
                def
                Nothing
    return $ mkClientEnv mgr gitlabApiBaseUrl

-- If failure response, print an error message. If any other unsuccessful
-- respones, error. Else do nothing.
onResp :: (Serv.Response -> String) -> Either ClientError a -> IO (Maybe a)
onResp errMsg resp = case resp of
    Left (FailureResponse _ fResp) -> case statusCode (responseStatusCode fResp) of
        -- Unauthorized
        401 -> error $ "GitLab request Unauthorized. Is the AccessToken valid?\n\n" ++ (show fResp)
        404 -> putStrLn (errMsg fResp) >> return Nothing
        _   -> error (show fResp)
    Left e                      -> error (show e)
    Right a                     -> return (Just a)
