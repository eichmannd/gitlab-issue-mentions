{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds, TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import qualified Data.Text          as T
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified System.Environment as Env
import           Control.Monad      ( forever, forM_, guard, when )
import           Control.Exception
import           Data.Aeson         ( Object, decode, (.:) )
import           Data.Aeson.Types   ( parseMaybe )
import           Data.List          ( nub )
import           Data.Maybe         ( mapMaybe )
import           Data.Text          ( Text )
import           Data.Proxy
import           Servant.API
import           Servant.Client     as Serv
import           System.Console.GetOpt
import           System.IO          ( hPutStrLn, stderr )
import           System.IO.Temp     ( withSystemTempDirectory )
import           Text.Read          ( readMaybe )
import           Text.Regex.PCRE
import           Control.Monad.IO.Class
import           Control.Concurrent
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Servant

import Git hiding (Commit)
import GitLab

--
-- Main
--

main :: IO ()
main = do
    opts <- getOptions
    if optOneShot opts
    then addNewQuotes opts
    else do
        -- Use a full MVar to cause an initial call to addNewQuotes.
        doAddQuotesMV <- newMVar ()
        _ <- forkIO . forever $ do
            takeMVar doAddQuotesMV
            (addNewQuotes opts)
                -- Catch all exceptions at the top level.
                `catch` (\ (e :: SomeException) -> do
                    let err = show e
                    hPutStrLn stderr ("Warning: addNewQuotes failed: " ++ err)
                    return ())
        run (optPort opts) (mainApp doAddQuotesMV)

--
-- Web Server Stuff
--

mainApp :: MVar () -> Application
mainApp doAddQuotesMV = serve (Proxy @WebAPI) (server doAddQuotesMV)

data Commit = Commit SHA Text Date
    deriving (Eq, Ord)

type WebAPI = "AddGitLabCommitQuotes" :> Get '[JSON] NoContent

server :: MVar () -> Server WebAPI
server doAddQuotesMV = do
    -- Use non-blocking tryPutMVar as we only one to queue at most 1 invocation.
    liftIO $ do
        queueSucceeded <- tryPutMVar doAddQuotesMV ()
        when (not queueSucceeded)
             (putStrLn "# AddGitLabCommitQuotes requested but is already queued. Ignoring.")
    return NoContent

--
-- Add new commit quotes to GitLab
--

-- | A single run of searching for the latest unquoted commits and adding those
-- missing quotes to gitlab.
addNewQuotes :: Options -> IO ()
addNewQuotes opts = do

    putStrLn "# Starting AddGitLabCommitQuotes"

    -- Get project info
    let token     = optAccessToken opts
        projectId = optProjectId   opts
    glEnv <- makeGitLabEnv (optBaseUrl opts)
    Just prjInfo <- onResp (\ fResp -> "\nFailed to get project info:" ++ show fResp ++ "\n")
                =<< runClientM (getProject token projectId) glEnv

    -- With new clone.
    withGitRepo (T.unpack $ prjRepoHttpUrl prjInfo) $ \ repo -> do
        latestUnquotedCommits <- findLatestUnquotedCommits
                                   token projectId glEnv repo (optRevRange opts)
        putStrLn $ "# Found " ++ show (length latestUnquotedCommits)
                 ++ " unquoted commits."
        mapM_
            (addQuotes (optDryRun opts) token projectId glEnv)
            -- Reverse to start from oldest commit.
            (reverse latestUnquotedCommits)

    putStrLn "# Finished AddGitLabCommitQuotes"

-- Get master commits that contain at least one issue reference, up to the
-- first commit that that *has* been quoted in GitLab (i.e. an issue comment
-- quoting the commit exists in a referenced issue).
findLatestUnquotedCommits :: AccessToken -> ProjectId -> ClientEnv -> GitRepo -> String -> IO [Commit]
findLatestUnquotedCommits token projectId env repo revRange = do
    masterCommits <- getCommits repo revRange
    filter (\ (Commit _ msg _) -> mentionedIssues msg /= [])
       <$> takeMWhileM
            (fmap not . commitHasEvidenceOfQuoting token projectId env)
            masterCommits

-- | Quote the commit on GitLab (as issue notes/comments) on all issues
-- referenced in the commit message.
addQuotes :: Bool -> AccessToken -> ProjectId -> ClientEnv -> Commit -> IO ()
addQuotes
  dryRun
  token
  projectId
  env
  (Commit sha msg date)
  = forM_ (mentionedIssues msg) $ \ iid -> do
    let body =  T.concat
            [ glQuoteTag sha
            , "\nmentioned in commit "
            , T.take 8 (Git.getSHA sha), ":"
            -- Multiline block quote
            , "\n>>>\n"
            -- Replace "#" with "#<!-- -->" so that GitLab doesn't recognise
            -- any issue references. This is done to stop GitLab from createing
            -- system notes "Moe Bot mentioned issue #123..."
            , T.replace "#" "#<!-- -->" msg
            , "\n>>>"
            ]
    if dryRun
    then putStrLn $ "Dry Run create issue note "
                  ++ T.unpack (Git.getSHA sha)
                  ++ " -> #" ++ show (unIssueIid iid)
    else do
        _ <- onResp (\ fResp -> "\nFailed to create note for issue #"
            ++ show (unIssueIid iid)
            -- TODO Will we always retry such issues on subsequent runs?
            ++ ". Assuming this issue does not exist:\n\n"
            ++ show fResp ++ "\n")
            =<< runClientM (createIssueNote token projectId iid body date) env
        return ()

-- | Get all commits on master.
getCommits :: GitRepo -> String -> IO [IO Commit]
getCommits repo revRange = do
    shas <- Git.commitLog repo (Git.CommitRef (Git.Ref (T.pack revRange)))
    let shaToCommit sha = Commit sha
                        <$> Git.commitMessage    repo (Git.CommitSha sha)
                        <*> Git.commitAuthorDate repo (Git.CommitSha sha)
    return $ fmap shaToCommit shas

-- | Check for evidence that a commit has already been quoted in GitLab.
-- Evidence is any quote of the commit on a GitLab issue reference by the commit.
commitHasEvidenceOfQuoting :: AccessToken -> ProjectId -> ClientEnv -> Commit -> IO Bool
commitHasEvidenceOfQuoting token projectId env (Commit sha msg _) = do
    -- Get issues mentioned by this commit.
    let issues = mentionedIssues msg
    -- Search all issues for any evidence.
    let findEvidence []     = return False
        findEvidence (i:is) = do
            -- Query GitLab notes for issue i
            -- If any note quotes this commit then return true,
            -- else keep searching.
            resp <- onResp (\ _ -> "Failed to get notes from issue #"
                                   ++ show (unIssueIid i)
                                   ++ ". Assuming the issue doesn't exist")
                    =<< runClientM (getIssueNotes token projectId i) env
            case resp of
                Nothing     -> return False
                Just notes  -> if any ((Just sha ==)
                                        . shaFromNoteBody
                                        . noteBody)
                                      notes
                    then return True
                    else findEvidence is
    findEvidence issues

-- | Extract all issues referenced by a text (i.e. matching "#[0-9]+")
mentionedIssues :: Text -> [IssueIid]
mentionedIssues msg = nub $ mapMaybe
                        (fmap IssueIid . readMaybe . (!!1))
                        (T.unpack msg =~ ("#([0-9]+)" :: String) :: [[String]])

-- | Extract the commit SHA from a text if it contains a quote tag.
shaFromNoteBody :: Text -> Maybe SHA
shaFromNoteBody note
    = case (T.unpack note =~ ("<!--(.+)-->" :: String) :: [[String]]) of
        [_, objStr]:_ -> (decode (BS.pack objStr) :: Maybe Object) >>= (parseMaybe $ \ obj -> do
                            noteType :: String <- obj .: "note_type"
                            guard $ noteType == "commit_mentions_issue"
                            sha <- obj .: "commit"
                            return (SHA $ T.pack sha))
        _ -> Nothing

-- | Get the tag text to place in the GitLab comment quoting a commit e.g.:
--       glQuoteTag "1a2b3c4d5e1a2b3c4d5e1a2b3c4d5e1a2b3c4d5e"
--          == "<!-- { \"note_type\": \"commit_mentions_issue\"", \"commit\": \"1a2b3c4d5e1a2b3c4d5e1a2b3c4d5e1a2b3c4d5e\" } -->"
glQuoteTag :: SHA -> Text
glQuoteTag sha = T.concat
    [ "<!-- { \"note_type\": \"commit_mentions_issue\", \"commit\": \""
    , getSHA sha
    , "\" } -->"
    ]

-- glQuoteTagPrefix, glQuoteTagSuffix :: String
-- glQuoteTagPrefix = "<!-- GLIM commit "
-- glQuoteTagSuffix = " mentions issue -->"

-- | Clone the repo into a temp dir.
withGitRepo :: String -> (GitRepo -> IO a) -> IO a
withGitRepo repoUrl cont = withSystemTempDirectory "gitrepo" $ \ dir ->
    cont =<< Git.cloneUrl repoUrl dir

-- | takeWhile but with some extra monads.
takeMWhileM :: Monad m => (a -> m Bool) -> [m a] -> m [a]
takeMWhileM _    []     = return []
takeMWhileM predicate (mx:mxs) = do
    x <- mx
    inc <- predicate x
    if inc
        then (x:) <$> takeMWhileM predicate mxs
        else return []

--
-- Options
--

data Options = Options
    { optPort           :: Int
    , optAccessToken    :: AccessToken
    , optProjectId      :: ProjectId
    , optBaseUrl        :: BaseUrl
    , optDryRun         :: Bool
    , optOneShot        :: Bool
    , optRevRange       :: String
    }

defaultOpts :: Options
defaultOpts = Options
    { optPort           = 9113
    , optAccessToken    = err "token" "-t"
    , optProjectId      = err "project" "-j"
    , optBaseUrl        = BaseUrl
                            { baseUrlScheme = Https
                            , baseUrlHost = err "gitlab host" "-h"
                            , baseUrlPort = 443
                            , baseUrlPath = "api/v4"
                            }
    , optDryRun         = False
    , optOneShot        = False
    , optRevRange       = "master"
    } where
        err x op = error $ "Must set " ++ x ++ " with " ++ op ++ "\n" ++ usageInfo "Options:" options

options :: [OptDescr (Options -> Options)]
options =
    [ Option []        ["test"]
        (NoArg (\ opts -> opts
            { optPort           = 9113
            , optProjectId      = ProjectId 9263725
            , optBaseUrl        = BaseUrl
                                    { baseUrlScheme = Https
                                    , baseUrlHost = "gitlab.com"
                                    , baseUrlPort = 443
                                    , baseUrlPath = "api/v4"
                                    }
        }))
        "Use the test project options. A token must be specified."

    , Option []        ["ghc"]
        (NoArg (\ opts -> opts
            { optPort           = 9113
            , optProjectId      = ProjectId 1
            , optBaseUrl        = BaseUrl
                                    { baseUrlScheme = Https
                                    , baseUrlHost = "gitlab.haskell.org"
                                    , baseUrlPort = 443
                                    , baseUrlPath = "api/v4"
                                    }
        }))
        "Use the ghc project options. A token must be specified."

    , Option ['p']     ["port"]
        (ReqArg (\ p opts -> opts { optPort = read p }) "PORT")
        "Server port number. Defaults to 9113."

    , Option ['t']     ["token"]
        (ReqArg (\ t opts -> opts { optAccessToken = AccessToken (T.pack t) }) "TOKEN")
        "GitLab API access token."

    , Option ['j']     ["project"]
        (ReqArg (\ t opts -> opts { optProjectId = ProjectId (read t) }) "ID")
        "GitLab project ID."

    , Option ['h']     ["gitlab-host"]
        (ReqArg (\ host opts -> opts { optBaseUrl
            = (optBaseUrl opts) { baseUrlHost = host } }) "HOST")
        "GitLab host name."

    , Option ['a']     ["gitlab-path"]
        (ReqArg (\ path opts -> opts { optBaseUrl
            = (optBaseUrl opts) { baseUrlPath = path } }) "PATH")
        "GitLab API base path."

    , Option ['d']     ["dry-run"]
        (NoArg (\ opts -> opts { optDryRun = True }))
        "Don't create any issue on GitLab, just print out what commits would be added."

    , Option ['1']     ["one-shot"]
        (NoArg (\ opts -> opts { optOneShot = True }))
        "Do quoting once and stop. Do not start a server."

    , Option []        ["rev-range"]
        (ReqArg (\ revRange opts -> opts { optRevRange = revRange }) "RANGE")
        "Which git revision range to search. defaults to 'master'."
    ]

getOptions ::  IO Options
getOptions = do
    argv <- Env.getArgs
    case getOpt Permute options argv of
        (o,[],[]  ) -> return $ foldl (flip id) defaultOpts o
        (_,ns,errs) -> ioError
                        . userError
                        . unlines
                        $ ["Unknown options: " ++ unwords ns]
                            ++ errs ++ [usageInfo header options]
    where header = "Options:"