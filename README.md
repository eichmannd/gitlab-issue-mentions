# gitlab-issue-mentions (GLIM)

This is a simple microservice that, when triggered, will "quote" commits on the master branch that reference a GitLab issue. I.e. if a new commit is pushed to the master branch with this commit message:

    Change a True to a False to fix issues #100 and #321

then, when triggered, this microservice will add a comment to issues #100 and #321 in the form:

    <!-- { "note_type": "commit_mentions_issue", "commit": "de15f5a7b7a0c7edeebc855f0a277e7795711286" } -->
    mentioned in commit de15f5a7:
    
    >>>
    Change a True to a False to fix issues #100 and #321
    >>>

## Run

First clone the repo.

    git clone https://gitlab.com/eichmannd/gitlab-issue-mentions.git
    cd gitlab-issue-mentions

To start the server for GHC run:

    cabal new-run glim -- --port 9113 --token <TOKEN> --project 1 --gitlab-host gitlab.haskell.org --gitlab-path api/v4

Or for short:

    cabal new-run glim -- --ghc --token <TOKEN>

Where `<TOKEN>` is a valid GitLab API token. You can generate one on your GitLab user settings page. In the case of GHC we use a dedicated user @moe-bot.

## Debugging Options

Use the `--dry-run` option to avoid adding any comments to GitLab.
Use the `--rev-range <commit1>..<commit2>` option to limit the range of commits searched for issue references.

## Triggering

The service will run (quote comments as necessary in GitLab) once immediatelly when the server is started, then whenever the `/AddGitLabCommitQuotes` end point is hit. You can e.g. trigger this with curl:

    curl --request GET http://localhost:9113/AddGitLabCommitQuotes

Or if you're running this on the gitlab.haskell.org server:

    curl --request GET http://gitlab.haskell.org:9113/AddGitLabCommitQuotes

### GitLab Webhook

GitLab can trigger this automatically when commits are pushed to master. In your browser, open the GitLab project page, and select Settings -> Integrations from the side panel. Here you can add a Webhook. enter the url `http://gitlab.haskell.org:9113/AddGitLabCommitQuotes`, check Push events and enter `master` for the branch name. Check `Enable SSL verification` and click `Add webhook`.

## Algorithm

The service is stateless and works as follows whever triggered:

  1. Clone the git repo.
  2. Search backwards through master commit history for the latest commit that is quoted on GitLab. This searched gitlab issues for existing quotes.
  3. Quote all newer commits. Specifically, add a comment to the issues referenced in the commit message. The comment will have its creation date set to the author date of the commit.